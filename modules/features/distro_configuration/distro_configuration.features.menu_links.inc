<?php
/**
 * @file
 * distro_configuration.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function distro_configuration_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: menu-footer-minor:node/3
  $menu_links['menu-footer-minor:node/3'] = array(
    'menu_name' => 'menu-footer-minor',
    'link_path' => 'node/3',
    'router_path' => 'node/%',
    'link_title' => 'Privacy Policy',
    'options' => array(),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '0',
  );
  // Exported menu link: menu-footer-minor:node/4
  $menu_links['menu-footer-minor:node/4'] = array(
    'menu_name' => 'menu-footer-minor',
    'link_path' => 'node/4',
    'router_path' => 'node/%',
    'link_title' => 'Terms and Conditions',
    'options' => array(
      'attributes' => array(
        'title' => 'Terms and Conditions',
      ),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '0',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Privacy Policy');
  t('Terms and Conditions');


  return $menu_links;
}
