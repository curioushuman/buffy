<?php
/**
 * @file
 * distro_configuration.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function distro_configuration_user_default_roles() {
  $roles = array();

  // Exported role: administrator
  $roles['administrator'] = array(
    'name' => 'administrator',
    'weight' => '2',
  );

  // Exported role: editor
  $roles['editor'] = array(
    'name' => 'editor',
    'weight' => '3',
  );

  // Exported role: publisher
  $roles['publisher'] = array(
    'name' => 'publisher',
    'weight' => '4',
  );

  // Exported role: seo
  $roles['seo'] = array(
    'name' => 'seo',
    'weight' => '5',
  );

  return $roles;
}
