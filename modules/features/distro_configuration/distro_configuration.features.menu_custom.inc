<?php
/**
 * @file
 * distro_configuration.features.menu_custom.inc
 */

/**
 * Implements hook_menu_default_menu_custom().
 */
function distro_configuration_menu_default_menu_custom() {
  $menus = array();

  // Exported menu: menu-footer
  $menus['menu-footer'] = array(
    'menu_name' => 'menu-footer',
    'title' => 'Footer',
    'description' => 'Footer menu (if different to main menu)',
  );
  // Exported menu: menu-footer-minor
  $menus['menu-footer-minor'] = array(
    'menu_name' => 'menu-footer-minor',
    'title' => 'Footer minor',
    'description' => 'Minor links in the footer e.g. privacy and terms',
  );
  // Exported menu: menu-secondary-menu
  $menus['menu-secondary-menu'] = array(
    'menu_name' => 'menu-secondary-menu',
    'title' => 'Secondary menu',
    'description' => '',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Footer');
  t('Footer menu (if different to main menu)');
  t('Footer minor');
  t('Minor links in the footer e.g. privacy and terms');
  t('Secondary menu');


  return $menus;
}
