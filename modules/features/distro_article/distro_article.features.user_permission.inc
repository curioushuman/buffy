<?php
/**
 * @file
 * distro_article.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function distro_article_user_default_permissions() {
  $permissions = array();

  // Exported permission: create article content
  $permissions['create article content'] = array(
    'name' => 'create article content',
    'roles' => array(
      0 => 'administrator',
      1 => 'editor',
      2 => 'publisher',
    ),
    'module' => 'node',
  );

  // Exported permission: create article content on assigned domains
  $permissions['create article content on assigned domains'] = array(
    'name' => 'create article content on assigned domains',
    'roles' => array(
      0 => 'administrator',
      1 => 'editor',
      2 => 'publisher',
    ),
    'module' => 'domain',
  );

  // Exported permission: delete any article content
  $permissions['delete any article content'] = array(
    'name' => 'delete any article content',
    'roles' => array(
      0 => 'administrator',
      1 => 'editor',
    ),
    'module' => 'node',
  );

  // Exported permission: delete article content on assigned domains
  $permissions['delete article content on assigned domains'] = array(
    'name' => 'delete article content on assigned domains',
    'roles' => array(
      0 => 'administrator',
      1 => 'editor',
    ),
    'module' => 'domain',
  );

  // Exported permission: delete own article content
  $permissions['delete own article content'] = array(
    'name' => 'delete own article content',
    'roles' => array(
      0 => 'administrator',
      1 => 'editor',
    ),
    'module' => 'node',
  );

  // Exported permission: edit any article content
  $permissions['edit any article content'] = array(
    'name' => 'edit any article content',
    'roles' => array(
      0 => 'administrator',
      1 => 'editor',
      2 => 'seo',
    ),
    'module' => 'node',
  );

  // Exported permission: edit own article content
  $permissions['edit own article content'] = array(
    'name' => 'edit own article content',
    'roles' => array(
      0 => 'administrator',
      1 => 'editor',
      2 => 'publisher',
    ),
    'module' => 'node',
  );

  // Exported permission: skip article content approval prior their publication
  $permissions['skip article content approval prior their publication'] = array(
    'name' => 'skip article content approval prior their publication',
    'roles' => array(
      0 => 'administrator',
      1 => 'editor',
    ),
    'module' => 'content_approval',
  );

  // Exported permission: update article content on assigned domains
  $permissions['update article content on assigned domains'] = array(
    'name' => 'update article content on assigned domains',
    'roles' => array(
      0 => 'administrator',
      1 => 'editor',
      2 => 'seo',
    ),
    'module' => 'domain',
  );

  return $permissions;
}
