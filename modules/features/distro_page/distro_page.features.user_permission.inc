<?php
/**
 * @file
 * distro_page.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function distro_page_user_default_permissions() {
  $permissions = array();

  // Exported permission: create page content
  $permissions['create page content'] = array(
    'name' => 'create page content',
    'roles' => array(
      0 => 'administrator',
      1 => 'editor',
      2 => 'publisher',
    ),
    'module' => 'node',
  );

  // Exported permission: create page content on assigned domains
  $permissions['create page content on assigned domains'] = array(
    'name' => 'create page content on assigned domains',
    'roles' => array(
      0 => 'administrator',
      1 => 'editor',
      2 => 'publisher',
    ),
    'module' => 'domain',
  );

  // Exported permission: delete any page content
  $permissions['delete any page content'] = array(
    'name' => 'delete any page content',
    'roles' => array(
      0 => 'administrator',
      1 => 'editor',
    ),
    'module' => 'node',
  );

  // Exported permission: delete own page content
  $permissions['delete own page content'] = array(
    'name' => 'delete own page content',
    'roles' => array(
      0 => 'administrator',
      1 => 'editor',
    ),
    'module' => 'node',
  );

  // Exported permission: delete page content on assigned domains
  $permissions['delete page content on assigned domains'] = array(
    'name' => 'delete page content on assigned domains',
    'roles' => array(
      0 => 'administrator',
      1 => 'editor',
    ),
    'module' => 'domain',
  );

  // Exported permission: edit any page content
  $permissions['edit any page content'] = array(
    'name' => 'edit any page content',
    'roles' => array(
      0 => 'administrator',
      1 => 'editor',
      2 => 'seo',
    ),
    'module' => 'node',
  );

  // Exported permission: edit own page content
  $permissions['edit own page content'] = array(
    'name' => 'edit own page content',
    'roles' => array(
      0 => 'administrator',
      1 => 'editor',
      2 => 'publisher',
    ),
    'module' => 'node',
  );

  // Exported permission: skip page content approval prior their publication
  $permissions['skip page content approval prior their publication'] = array(
    'name' => 'skip page content approval prior their publication',
    'roles' => array(
      0 => 'administrator',
      1 => 'editor',
    ),
    'module' => 'content_approval',
  );

  // Exported permission: update page content on assigned domains
  $permissions['update page content on assigned domains'] = array(
    'name' => 'update page content on assigned domains',
    'roles' => array(
      0 => 'administrator',
      1 => 'editor',
      2 => 'seo',
    ),
    'module' => 'domain',
  );

  return $permissions;
}
