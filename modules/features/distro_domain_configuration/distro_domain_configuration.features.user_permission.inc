<?php
/**
 * @file
 * distro_domain_configuration.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function distro_domain_configuration_user_default_permissions() {
  $permissions = array();

  // Exported permission: access domain navigation
  $permissions['access domain navigation'] = array(
    'name' => 'access domain navigation',
    'roles' => array(
      0 => 'administrator',
      1 => 'editor',
      2 => 'publisher',
      3 => 'seo',
    ),
    'module' => 'domain_nav',
  );

  // Exported permission: access domain settings form
  $permissions['access domain settings form'] = array(
    'name' => 'access domain settings form',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'domain_settings',
  );

  // Exported permission: access inactive domains
  $permissions['access inactive domains'] = array(
    'name' => 'access inactive domains',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'domain',
  );

  // Exported permission: administer domains
  $permissions['administer domains'] = array(
    'name' => 'administer domains',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'domain',
  );

  // Exported permission: assign domain editors
  $permissions['assign domain editors'] = array(
    'name' => 'assign domain editors',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'domain',
  );

  // Exported permission: delete domain content
  $permissions['delete domain content'] = array(
    'name' => 'delete domain content',
    'roles' => array(
      0 => 'administrator',
      1 => 'editor',
    ),
    'module' => 'domain',
  );

  // Exported permission: edit domain content
  $permissions['edit domain content'] = array(
    'name' => 'edit domain content',
    'roles' => array(
      0 => 'administrator',
      1 => 'editor',
    ),
    'module' => 'domain',
  );

  // Exported permission: publish from assigned domain
  $permissions['publish from assigned domain'] = array(
    'name' => 'publish from assigned domain',
    'roles' => array(
      0 => 'administrator',
      1 => 'editor',
    ),
    'module' => 'domain',
  );

  // Exported permission: publish from default domain
  $permissions['publish from default domain'] = array(
    'name' => 'publish from default domain',
    'roles' => array(
      0 => 'administrator',
      1 => 'editor',
    ),
    'module' => 'domain',
  );

  // Exported permission: publish to any assigned domain
  $permissions['publish to any assigned domain'] = array(
    'name' => 'publish to any assigned domain',
    'roles' => array(
      0 => 'administrator',
      1 => 'editor',
    ),
    'module' => 'domain',
  );

  // Exported permission: set domain access
  $permissions['set domain access'] = array(
    'name' => 'set domain access',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'domain',
  );

  // Exported permission: view unpublished domain content
  $permissions['view unpublished domain content'] = array(
    'name' => 'view unpublished domain content',
    'roles' => array(
      0 => 'administrator',
      1 => 'editor',
      2 => 'publisher',
      3 => 'seo',
    ),
    'module' => 'domain',
  );

  return $permissions;
}
