<?php
/**
 * @file
 * distro_domain_configuration.domains.inc
 */

/**
 * Implements hook_domain_default_domains().
 */
function distro_domain_configuration_domain_default_domains() {
$domains = array();
  $domains['angel_local'] = array(
  'subdomain' => 'angel.local',
  'sitename' => 'Exhibit B',
  'scheme' => 'http',
  'valid' => '1',
  'weight' => '1',
  'is_default' => '0',
  'machine_name' => 'angel_local',
);
  $domains['buffy_local'] = array(
  'subdomain' => 'buffy.local',
  'sitename' => 'Exhibit A',
  'scheme' => 'http',
  'valid' => '1',
  'weight' => '-1',
  'is_default' => '1',
  'machine_name' => 'buffy_local',
);
  $domains['spike_local'] = array(
  'subdomain' => 'spike.local',
  'sitename' => 'Exhibit C',
  'scheme' => 'http',
  'valid' => '1',
  'weight' => '2',
  'is_default' => '0',
  'machine_name' => 'spike_local',
);

return $domains;
}

/**
 * Implements hook_domain_conf_default_variables().
 */
function distro_domain_configuration_domain_conf_default_variables() {
$domain_variables = array();
  $domain_variables['angel_local'] = array();
  $domain_variables['buffy_local'] = array();
  $domain_variables['spike_local'] = array();

return $domain_variables;
}

/**
 * Implements hook_domain_theme_default_themes().
 */
function distro_domain_configuration_domain_theme_default_themes() {
$domain_themes = array();
  $domain_themes['angel_local'] = array();
  $domain_themes['buffy_local'] = array();
  $domain_themes['spike_local'] = array();

return $domain_themes;
}
