; API
; ---

api = 2

; CORE
; ----

core = "7.10"
projects[drupal][type] = "core"
projects[drupal][version] = "7.10"

; CONTRIB MODULES
; Please keep these in alpahebtical order
; ---------------

projects[auto_nodetitle][subdir] = "contrib"
projects[auto_nodetitle][version] = "1.0"

projects[boxes][subdir] = "contrib"
projects[boxes][version] = "1.0-beta6"

projects[content_approval][subdir] = "contrib"
projects[content_approval][version] = "1.0"

projects[content_lock][subdir] = "contrib"
projects[content_lock][version] = "1.1"

projects[context][subdir] = "contrib"
projects[context][version] = "3.0-beta2"

projects[ctools][subdir] = "contrib"
projects[ctools][version] = "1.0-rc1"

projects[date][subdir] = "contrib"
projects[date][version] = "2.0-alpha5"

projects[domain][subdir] = "contrib"
projects[domain][version] = "3.2"

projects[entity][subdir] = "contrib"
projects[entity][version] = "1.0-rc1"

projects[features][subdir] = "contrib"
projects[features][version] = "1.0-beta6"

projects[field_group][subdir] = "contrib"
projects[field_group][version] = "1.1"

projects[google_analytics][subdir] = "contrib"
projects[google_analytics][version] = "1.2"

projects[jquery_update][subdir] = "contrib"
projects[jquery_update][version] = "2.2"

projects[libraries][subdir] = "contrib"
projects[libraries][version] = "1.0"

projects[media][subdir] = "contrib"
projects[media][version] = "1.0-rc2"

projects[metatag][subdir] = "contrib"
projects[metatag][version] = "1.0-alpha4"

projects[menu_block][subdir] = "contrib"
projects[menu_block][version] = "2.2"
projects[menu_block][patch][] = "http://drupal.org/files/issues/menu_block_include_parent_d7.patch"

projects[module_filter][subdir] = "contrib"
projects[module_filter][version] = "1.6"

projects[mongodb][subdir] = "contrib"
projects[mongodb][version] = "1.0-beta2"

projects[pathauto][subdir] = "contrib"
projects[pathauto][version] = "1.0"

projects[panels][subdir] = "contrib"
projects[panels][version] = "3.0-alpha3"

projects[page_title][subdir] = "contrib"
projects[page_title][version] = "2.5"

projects[redirect][subdir] = "contrib"
projects[redirect][version] = "1.0-beta4"

projects[strongarm][subdir] = "contrib"
projects[strongarm][version] = "2.0-beta2"

projects[token][subdir] = "contrib"
projects[token][version] = "1.0-beta7"

projects[transliteration][subdir] = "contrib"
projects[transliteration][version] = "3.0"

projects[uuid][subdir] = "contrib"
projects[uuid][version] = "1.0-alpha2"

projects[uuid_features][subdir] = "contrib"
projects[uuid_features][version] = "1.0-alpha1"

projects[views][subdir] = "contrib"
projects[views][version] = "3.0"

projects[wysiwyg][subdir] = "contrib"
projects[wysiwyg][version] = "2.1"

projects[xmlsitemap][subdir] = "contrib"
projects[xmlsitemap][version] = "2.0-rc1"

; THEMES

projects[omega][subdir] = "contrib"
projects[omega][version] = "3.0"

projects[rubik][subdir] = "contrib"
projects[rubik][version] = "4.0-beta7"

projects[tao][subdir] = "contrib"
projects[tao][version] = "3.0-beta4"

; LIBRARIES

libraries[ckeditor][download][type] = "get"
libraries[ckeditor][download][url] = "http://download.cksource.com/CKEditor/CKEditor/CKEditor%203.6.2/ckeditor_3.6.2.tar.gz"