#!/bin/bash
# Use proxy (e.g. squid) to speed up regular builds
http_proxy="http://localhost:3128/" drush make --no-core --contrib-destination=. devel.construct .
